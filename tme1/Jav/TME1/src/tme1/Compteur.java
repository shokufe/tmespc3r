package tme1;

public class Compteur {
	private int cpt;

	public Compteur(int cpt) {
		this.cpt = cpt;
	}

	public synchronized int getCpt() {
		return cpt;
	}

	public synchronized void decrement() {
		//synchronized (this) {
			cpt--;
		//}

	}

	public synchronized boolean EndCons() {
		return (cpt == 0);
	}

}
