package tme1;

import java.util.ArrayList;

public class Tapis {
	public int capacity;

	ArrayList<Paquet> paquetList;
	private Object mutex = new Object();
	public Tapis(int capacity) {
		this.capacity = capacity;
		paquetList = new ArrayList<>(capacity);
	}

	/**
	 *  enfiler le paquet / methode appel� par le producteur
	 *   **/
	public  void enfiler(Paquet pq) throws InterruptedException {

		synchronized (mutex) {
			// checking full or not
			while (isFull()) {
				mutex.wait();
			}
			paquetList.add(pq);
			mutex.notifyAll();

		}
	}
	/**
	 *  defiler un paquet / methode appel� par le consommateur
	 *   **/
	public Paquet defiler(Compteur cpt)  {
		
		Paquet pobj;
		synchronized (mutex) {
			// checking empty or not
			while (isEmpty()) {
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			pobj = paquetList.remove(0);
			cpt.decrement();
			mutex.notifyAll();
			return pobj;
		}
	}
	/**
	 * 
	 * @return current size
	 */
	public int size() {
		return paquetList.size();
	}
	/**
	 * chekk empty
	 * @return
	 */
	public synchronized boolean isEmpty() {
		return (paquetList.size() == 0);
	}

	/**
	 * check full
	 * @return
	 */
	public synchronized boolean isFull() {
		return (size() == capacity);
	}

}