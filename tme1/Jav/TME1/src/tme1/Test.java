package tme1;


public class Test {

	 public static void main(String[] args) {
	       
	        int n = 4;
	        int m = 3;
	        // cr�ation tapis
	        Tapis tp = new Tapis(10);

	        Thread[] producers = new Thread[n];
	        Thread[] consumers = new Thread[m];
	        
	        int cible = 2;
	        Compteur cptr = new Compteur(cible * n);
	        
	        
	        /**pool de threads for producer 
	         * 
	         * lancement des producteur
	         * **/
	       for(int i = 0;i<n;i++) {
	        
	        producers[i]= new Thread(new Producteur("pomme", cible,tp));
	        producers[i].start();
	       }
	        /***pool de threads for consumator
	         * 
	         * lancement des consommateurs
	         * **/
	        for(int i=0;i<m ;i++){
	        	
	        	consumers[i] = new Thread(new Consommateur(tp, cptr));
	        	consumers[i].start();
	        }

	 
	        
	 // Attente terminaison des fils
	  
	 for (int i=0; i< n ; i++) {
		 try {
			// tp.wake();
			producers[i].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	 
	 for (int i=0; i<m;i++) {
		 try {
			consumers[i].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
		 
	 }
}
