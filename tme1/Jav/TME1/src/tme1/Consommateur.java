package tme1;

public class Consommateur implements Runnable {

	
	public static int val = 0;
	public int id;
	Tapis tapis;
	public Compteur cpt;

	public Consommateur(Tapis tapis, Compteur cpt) {
		id = val++;
		this.tapis=tapis;
		this.cpt = cpt;
	}

	public  void run() {
		Paquet pq = null;
		while (cpt.getCpt()>0) {
				pq = tapis.defiler(cpt);
				System.out.println("C"+id +" mange"+" " + pq.toString());
				//cpt.decrement();
		}
	}

}
