#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#define NCONC 2;
#define MPROD 2;
#define TAPISCAPACITE 2;
///
//last version
//pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutexc;
pthread_cond_t	canProduce = PTHREAD_COND_INITIALIZER;
pthread_cond_t	canConsume = PTHREAD_COND_INITIALIZER;


typedef struct Paquet Paquet;
struct Paquet{
    char*  nom; //data
    Paquet *nextpq;
};
typedef struct Tapis Tapis;
struct Tapis{
    int capacity ;
    int actualsize ;
    Paquet *first;//pointer to the head of queue
    Paquet *next; //pointer to next paquet in list
};
typedef struct ProdArgs ProdArgs;// parameters to send to producer
struct ProdArgs {
    int id;
    Tapis *t;
    int cible;
};

typedef struct ConsArgs ConsArgs;// parameters to send to consumer
struct ConsArgs {
    int id;
    Tapis *t;
    int compteur;
};

Tapis* defiler(Tapis *tp,int idth, int cpt){
     char *pqName;
    if( tp->first==NULL ){
        printf("tapis est empty\n");
        pthread_cond_signal(&canProduce);
        pthread_cond_wait(&canConsume, &mutexc);
    }
     pthread_mutex_lock (&mutexc);
    if(tp->first !=NULL)  //queue is not empty
    {
       Paquet *pq = tp->first;
       pqName = pq->nom;
       printf("consumer%d mange %s\n",idth,pqName);
     
       while(tp->next !=NULL ){
        tp->first=tp->next;
        tp->next=tp->first->nextpq;
       }
         
    }
  
     pthread_cond_signal(&canProduce);
     pthread_mutex_unlock (&mutexc);
    return tp;
    }


////////////////////////////////
Tapis * enfiler(Tapis * tp,Paquet *pq,int g,int idProd){ 
 pthread_mutex_lock (&mutexc);
   if(tp->actualsize==tp->capacity){
      printf("tapis is full ,actualsize=%d \n",tp->actualsize);
      pthread_cond_signal (&canConsume);
      pthread_cond_wait(&canProduce, &mutexc);
   }
   
    if(tp->first != NULL) // la file n'est pas vide
    {
        //Paquet *paquet = tp->first;
        while(tp->next !=NULL)
        {
            tp->first=tp->next;
            tp->next=tp->next->nextpq;
        }
        tp->first=pq;
        tp->next =NULL;
        pq->nextpq=NULL;
        ++tp->actualsize;
    }else{
        tp->first = pq;
        ++tp->actualsize;
        tp->next=NULL;
        pq->nextpq=NULL;
    }
    char* ss=tp->first->nom;
    printf("producer%d produce %s\n",idProd,ss);
    pthread_cond_signal(&canConsume);
    pthread_mutex_unlock (&mutexc);
    
    return tp;
}
void *consumer(void * arg){
    ConsArgs *data;
    data=arg;
    int cpt = data->compteur;
    Tapis *tp= data->t;
    int thread_id = data->id;
    char *pqContent; 
    int kk=1;
//it will consume until compteur.control empty or full will hold in defiler
      for(int y=1;y<=cpt;y++){
         tp=defiler(tp,thread_id,y);
    }
   
}
void* productor(void *arg){
   struct ProdArgs *data;
    data=arg;
    int cible=data->cible;
    Tapis *tp2= data->t;
    int idProd = data->id;     
   for(int h=1;h<=cible;h++){
      char *name;
      name="pomme";
      char *str2=malloc(sizeof(str2));
      sprintf(str2, "%d", h);
      char * str3 = (char *) malloc(1 + strlen(name)+ strlen(str2) );
      strcpy(str3, name);
      strcat(str3, str2);
      Paquet *pqt =malloc(sizeof(*pqt));
      pqt->nom = str3;
      tp2=enfiler(tp2,pqt,h,idProd);
    }

}
int main() {
    pthread_t ThProducer[2];
    pthread_t ThConsumator[2];
    Tapis* tapis = malloc(sizeof(*tapis)); 
    tapis->capacity = 5;
    tapis->actualsize = 0;
    tapis->first = NULL;
    tapis->next =NULL;
    pthread_mutex_init(&mutexc,NULL);
    int cpt= 2;
    int cbl =2;

    for (int j=1;j<=2;j++){
        ProdArgs *args=malloc(sizeof(*args));
        void * params;
        args->id = j;
        args->t = tapis;
        args->cible = cbl;
        params=args;
        pthread_create(&ThProducer[j],NULL,productor,params); //it is not return actual size correct
    }
    for (int l=1;l<=2;l++){
        ConsArgs *cons=malloc(sizeof(*cons));
        void * paramsCons;
        cons->id = l;
        cons->t = tapis;
        cons->compteur  = cpt;
        paramsCons=cons;
        pthread_create(&ThConsumator[l],NULL,consumer,paramsCons);
    }

    pthread_join(ThProducer[1],NULL);
    pthread_join(ThProducer[2],NULL);
    pthread_join(ThConsumator[1],NULL);
    pthread_join(ThConsumator[2],NULL);


return 0;
}
