
use std::collections::VecDeque;
use  std ::sync::Arc;
use  std ::sync::Mutex;


const NB_THREAD :usize =10;
const NB_PROD :usize =10;
const NB_CONS :usize =10;
#[allow(dead_code)] 
struct Paquet{
	name :String
}
#[allow(dead_code)] 
struct Tapis {
	taille :usize ,
	elems :VecDeque<Paquet>
}

#[allow(unreachable_code)]
#[allow(dead_code)] 
#[allow(non_snake_case)]
#[allow(unused_assignments)]
fn defiler(tp: &Arc<Mutex<Tapis>>)->Paquet{
	let mut tp_tk = tp.lock().unwrap();
	let p = tp_tk.elems.pop_front().unwrap();
	p
}
#[allow(unreachable_code)]
#[allow(dead_code)] 
#[allow(non_snake_case)]
#[allow(unused_assignments)]
fn enfiler(tp: Arc<Mutex<Tapis>>, paquet:Paquet)->std::sync::Arc<std::sync::Mutex<Tapis>>{
	let mut tp_lk = tp.lock().unwrap();
	tp_lk.taille +=1;
	tp_lk.elems.push_back(paquet);	
    drop(tp_lk);
    tp
}
fn main() {
    println!("Hello, world!");
    let tapis= Arc::new(Mutex::new(Tapis {elems :VecDeque::new(),taille : 0}));
   
   let mut pool =Vec::new();
	//let mut hasContain = true;
	//create pool de threads producer
	for i in 0..NB_THREAD {
//	let mut tpClone =Arc::clone(&tapis);
let mut tpClone =Arc::clone(&tapis);
		pool.push(std::thread::spawn(
			move || {
				for j in 0..NB_PROD{
					//producer function
					let p = Paquet{name: format!("Object {}",j.to_string())};//create paquet name
				    println!("producer {} produce {}" , i.to_string(),p.name);
					tpClone=enfiler(tpClone, p);   //push 
				
				}
			}));
	};
//create pool de threads consummer
	for i in 0..NB_CONS{
		let tapis_clone = Arc::clone(&tapis);
		let hasContain = true;
		//create and push a thread
		println!("thread");
		pool.push(std::thread::spawn(
			move || {
	
			//until there are paquets in tapis ,it will pop a paquet 
			//	while tapis.taille >0 {
			    while hasContain{
			        let p = defiler(&tapis_clone);
					println!("consumer number {} consume {}" ,i.to_string(), p.name);
			    }
					
			
				
			}
		));     
	}
	//threads starts their jobs
	for h in pool{
		h.join().unwrap();
	}
    
}
