package main

import (
	"bufio"
	"fmt"
	"strings"
	"io"
	"os"
	"time"
	//"strconv"
)

type paquet struct {
	arrivee string
	depart  string
	duree   int
	Chanretour  chan paquetChan
	idtravaill int
}
type paquetChan struct {
	duration   int
	idtravaill int
}

func lecteur(lectStart chan int, travail chan string, endlect chan int,fileR *os.File) {
	<-lectStart
	//read line by line 
	r4 := bufio.NewReader(fileR)
	b4, err := r4.ReadString('\n')
	for err != io.EOF {
	//	fmt.Println(b4)
	travail <- b4 //send line to travailleur
		b4, err = r4.ReadString('\n')
	}
	endlect <- 0
}
func travailleur(travail chan string, cal chan paquet, reduct chan int, idtrav int) {
	for true {
		lineCon := <-travail		// recieve paquet
		// split the line by comma
		lineParts :=strings.Split(lineCon,",")
		arv :=lineParts[1]
		dep :=lineParts[2]
		dur :=0
		Chanretour := make(chan paquetChan)
		p := paquet{arv, dep, dur, Chanretour,idtrav}
		cal <- p
		//recieve from serverOfCalcul
		 newPq := <-Chanretour
		 if(idtrav==newPq.idtravaill){ // check the id of travailleur 
				reduct <- newPq.duration //send duration to reducteur
			}
				
	}
}
func serverDeCalcul(cal chan paquet, finish chan int) {
	for true {
		select {
		case <-finish:
			break
		case req := <-cal:
			go func(pq paquet) {
				oldDepar:=pq.depart
				oldArrive:=pq.arrivee
				oldChan :=pq.Chanretour
				idtravailleur :=pq.idtravaill
				dpr, _ := time.Parse("15:04:05", oldDepar)
				arr, _ := time.Parse("15:04:05", oldArrive)
				duration := dpr.Sub(arr) //calculate the duration depart-arrivee
				pResultat := paquetChan{ int(duration.Seconds()),idtravailleur}
				oldChan <- pResultat 
			}(req)
		}
	}
}

func reducteur(work chan int, endPro chan int, TotalDur int) {
	for true {
		select {
			//if he recieve signal end of program il will terminate and return the result
		case <-endPro:
			endPro <- TotalDur
		case durNew := <-work:
			TotalDur = TotalDur + durNew
		}
	}
}
func main() {
	//open file
	StopFiles, err := os.Open("./stop_times2.txt")
	if err != nil {
		fmt.Printf("there is probleme to open file \n ")
	}
    //create channels
	lectStart := make(chan int)
	travail := make(chan string,10)
	cal := make(chan paquet)
	reduct := make(chan int)
	endlect := make(chan int)
   //end of programme
	endPro := make(chan int)
	go lecteur(lectStart, travail, endlect,StopFiles)
	//two workers
	for j := 1; j < 3; j++ {
		go travailleur(travail, cal, reduct,j)
	}
	finish := make(chan int)
	go serverDeCalcul(cal, finish)
	TotalDur := 0
    go reducteur(reduct, endPro,TotalDur)
	lectStart <- 0
	<-endlect
	//finish the serverOf calcul for return 
	finish <- 0
	endPro <- 0
	res := <-endPro
    //print the final result in second
	fmt.Printf(" temps passe est (sec)%d\n",int(res))
}
