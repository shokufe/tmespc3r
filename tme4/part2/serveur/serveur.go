package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"

	st "../client/structures"
	tr "./travaux"
)

var ADRESSE = "localhost"

var pers_vide = st.Personne{Nom: "", Prenom: "", Age: 0, Sexe: "M"}

// type d'un paquet de personne stocke sur le serveur, n'implemente pas forcement personne_int (qui n'existe pas ici)
type personne_serv struct {
	identifier int
	Personne st.Personne
	status string 
	afaire []func(st.Personne) st.Personne
	//lecture chan message_lec
	//ligne int
	//CallMethod string
}
type message_recu struct{
	id int
	method string
	retourChan chan string
}

type message_lec struct{
	contenu int
	retour chan string
}
// cree une nouvelle personne_serv, est appelé depuis le client, par le proxy, au moment ou un producteur distant
// produit une personne_dist
func creer(id int) *personne_serv{
	p:=new (personne_serv)
	//p.identifier = id
	p.Personne=pers_vide
	p.status = "V"
	//p.ligne = rand.Intn(TAILLE_SOURCE)
	taches :=make([]func (st.Personne) st.Personne,0)
	p.afaire = taches
	return p

}

// Méthodes sur les personne_serv, on peut recopier des méthodes des personne_emp du client
// l'initialisation peut être fait de maniere plus simple que sur le client
// (par exemple en initialisant toujours à la meme personne plutôt qu'en lisant un fichier)
func (p *personne_serv) initialise() {
	// A FAIRE
	for i:=0; i< rand.Intn(6)+1 ; i++ {
		p.afaire = append(p.afaire, tr.UnTravail())
	}
	p.status ="R"
}

func (p *personne_serv) travaille() {
	// A FAIRE

	if len(p.afaire) == 0{
		p.status = "C"
		return
	}
	tach := p.afaire[0]
	p.Personne = tach(p.Personne)
	p.afaire = p.afaire[1:]

	
}

func (p *personne_serv) vers_string() string {
	return fmt.Sprintf(p.Personne.Nom, ", ",p.Personne.Prenom,", ",p.Personne.Age,", ",p.Personne.Sexe)
	
}

func (p *personne_serv) donne_statut() string {
	return p.status
}

// Goroutine qui maintient une table d'association entre identifiant
// et personne_serv il est contacté par les goroutine de gestion
// avec un nom de methode et un identifiant
// et il appelle la méthode correspondante de la personne_serv
// correspondante
func mainteneur(request chan message_recu) {
	//table association <id, personne_serv>
	for{
		req :=<- request
		persone := creer(req.id)
		methodd:=req.method;
		switch methodd{
		case "initialise":
			persone.initialise()
			req.retourChan <-"finish" 
		case "travaille":
			persone.travaille()
			req.retourChan <- "finish"
		case "vers_string":
			result:=persone.vers_string()
			req.retourChan <- result
		case "donne_satut":
			result:=persone.donne_statut()
			req.retourChan <- result
		default:
			os.Exit(23)			
	}
}
	
}

// Goroutine de gestion des connections
// elle attend sur la socketi un message content un nom de methode et un identifiant et appelle le mainteneur avec ces arguments
// elle recupere le resultat du mainteneur et l'envoie sur la socket, puis ferme la socket
func gere_connection(conn net.Conn, m chan message_recu) {
	continu := true

	for continu {
		message,_ :=bufio.NewReader(conn).ReadString('\n')
		brokeMess := strings.Split(message, "*")
		idrec, _	:= strconv.Atoi(brokeMess[0])
		methodrec 	:= brokeMess[1]
		repondre :=make(chan string)
		m<-message_recu{id:idrec , method:methodrec ,retourChan:repondre}
		result:=<-repondre
		if message !="\n"{
			conn.Write([]byte(result + strings.ToUpper(message)))

		}else{
			conn.Close()
			continu = false

		}
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Format: client <port>")
		return
	}
	port, _ := strconv.Atoi(os.Args[1]) // doit être le meme port que le client
	addr := ADRESSE + ":" + fmt.Sprint(port)
	// A FAIRE: creer les canaux necessaires, lancer un mainteneur
	ln, _ := net.Listen("tcp", addr) // ecoute sur l'internet electronique
	m :=make(chan message_recu)
	fmt.Println("Ecoute sur", addr)
	go mainteneur (m)
	for {
		conn, _ := ln.Accept() // recoit une connection, cree une socket 
		fmt.Println("Accepte une connection.")
		go gere_connection(conn,m) // passe la connection a une routine de gestion des connections
	}
}
