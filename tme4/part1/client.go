package main

import (
	"fmt"
	"math/rand"
	"os"
	"regexp"
	//"strconv"
	"time"
	"bufio"
	//"io/ioutil"
	"log"
	//"path/filepath"

	st "./structures" // contient la structure Personne
	tr "./travaux" // contient les fonctions de travail sur les Personnes
)

var ADRESSE string = "localhost"                           // adresse de base pour la Partie 2
var FICHIER_SOURCE string = "./donne.txt" // fichier dans lequel piocher des personnes
var TAILLE_SOURCE int = 5                         // inferieure au nombre de lignes du fichier, pour prendre une ligne au hasard
var TAILLE_G int = 5                                       // taille du tampon des gestionnaires
var NB_G int = 2                                           // nombre de gestionnaires
var NB_P int = 2                                           // nombre de producteurs
var NB_O int = 4                                           // nombre d'ouvriers
var NB_PD int = 2                                          // nombre de producteurs distants pour la Partie 2

var TAILLE_FILE = 27

var pers_vide = st.Personne{Nom: "", Prenom: "", Age: 0, Sexe: "M"} // une personne vide
//var gestionerFile []personne_int

// paquet de personne, sur lequel on peut travailler, implemente l'interface personne_int
type personne_emp struct {
	person	st.Personne
	status string
	afaire []func(st.Personne) st.Personne
	lec chan message_lec
	ligne int

}
 type message_lec struct{
	 contenu int
	 retour chan string
 }
 type message_proxy struct{
	id int
	methodName string
	//chan de retour
	retChan chan string
 }
// paquet de personne distante, pour la Partie 2, implemente l'interface personne_int
type personne_dist struct {
	id int
	proxy chan message_proxy
}

// interface des personnes manipulees par les ouvriers, les
type personne_int interface {
	initialise()          // appelle sur une personne vide de statut V, remplit les champs de la personne et passe son statut à R
	travaille()           // appelle sur une personne de statut R, travaille une fois sur la personne et passe son statut à C s'il n'y a plus de travail a faire
	vers_string() string  // convertit la personne en string
	donne_statut() string // renvoie V, R ou C
}

// fabrique une personne à partir d'une ligne du fichier des conseillers municipaux
// à changer si un autre fichier est utilisé
func personne_de_ligne(l string) st.Personne {
	separateur := regexp.MustCompile("\u0009") // oui, les donnees sont separees par des tabulations ... merci la Republique Francaise
	separation := separateur.Split(l, -1)
	naiss, _ := time.Parse("2/1/2006", separation[7])
	a1, _, _ := time.Now().Date()
	a2, _, _ := naiss.Date()
	agec := a1 - a2
	return st.Personne{Nom: separation[4], Prenom: separation[5], Sexe: separation[6], Age: agec}
}

// *** METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES ***
func (p *personne_emp) initialise() {
//	fmt.Println("INITIALISEEE ")
	resultChan:=make(chan string)
	line:= p.ligne
	mess:=message_lec{contenu: line, retour: resultChan}
	//fmt.Println("line sent", mess.contenu)
	p.lec <-mess
	li:=<-resultChan
	fmt.Println("*****************")
	fmt.Println("liiiiiiiiiiiii",li)
	fmt.Println("********************")
	p.person = personne_de_ligne(li)
	for i:=0; i< rand.Intn(6)+1 ; i++ {
		p.afaire = append(p.afaire, tr.UnTravail())
	}
	p.status = "R"
}
func (p *personne_emp) travaille() {

	p.afaire[0](p.person)
	p.afaire = p.afaire[1:]
	p.status ="C"
}
func (p *personne_emp) vers_string() string {
	return fmt.Sprint(p.person.Nom, ", ",p.person.Prenom,", ",p.person.Age,", ",p.person.Sexe)
}

func (p *personne_emp) donne_statut() string {
	return p.status
}

// *** METHODES DE L'INTERFACE personne_int POUR LES PAQUETS DE PERSONNES DISTANTES (PARTIE 2) ***
// ces méthodes doivent appeler le proxy (aucun calcul direct)

func (p personne_dist) initialise() {
	init:=make(chan string)
	messageProxy:=message_proxy{id: p.id , methodName:"initialise", retChan:init}
   p.proxy <-messageProxy
}

func (p personne_dist) travaille() {
	tra:=make(chan string)
	messageProxy:=message_proxy{id: p.id , methodName:"travaille", retChan:tra}
   p.proxy <-messageProxy
}

 func (p personne_dist) vers_string() string {
   conv:=make(chan string)
   messageProxy:=message_proxy{id: p.id , methodName:"vers_string", retChan:conv}
   p.proxy <-messageProxy
   result :=<-conv
   return result
 }

 func (p personne_dist) donne_statut() string {
	 stat :=make(chan string)
	 messageProxy:=message_proxy{id: p.id , methodName:"donne_statut", retChan:stat}
	p.proxy <-messageProxy
	result :=<-stat
	return result
}

// *** CODE DES GOROUTINES DU SYSTEME ***

// Partie 2: contacté par les méthodes de personne_dist, 
//le proxy appelle la méthode à travers le réseau et 
//récupère le résultat
// il doit utiliser une connection TCP sur le port donné en ligne de commande

func proxy(port int,proxyChan chan message_proxy, find chan int) {
	// if len ( os.Args ) < 3 {
	// 	return }
	// 	add:= os.Args[1] + ":" +os.Args[2]
	// 	conn,err := net.Dial("tcp",add)
	// 	if err !=nil{
	// 		return
	// 	}
	// reader :=bufio.NewReader(os.Stdin)
	// continu :=true	
	// for continu {
	// 	chain,_ :=reader.ReadString('\n')
	// 	if chain == "\n" {
	// 		fmt.Fprintf(conn, fmt.Sprint(chaine))
	// 		continu =falseconn.Close()
	// 	}
	// 	else{
	// 		fmt.Print("Envoi:", chaine)
	// 		reponse,_ :=bufio.NewReader(conn).ReadString('\n')
	// 		reponse = string.TrimSuffix(reponse, "\n")
	// 		fmt.Println("Recoit:",reponse)
	// 	}
	// 	}
		
		
}

// Partie 1 : contacté par la méthode initialise() de personne_emp, 
// récupère une ligne donnée dans le fichier source
func lecteur(lire chan message_lec) {

	for{
		mes:=<-lire
		//fmt.Println("Lecteur contacte par line", mes.contenu)
		file, err :=os.Open("conseillers-municipaux.txt")
		if err != nil {
			log.Fatal(err)
		}
		scanner := bufio.NewScanner(file)
		_ = scanner.Scan()

		for i :=0; i<mes.contenu; i++ {
			_=scanner.Scan()
		}
		result := scanner.Scan()
		if result ==false {
			log.Fatal(err)
		}else{
			//fmt.Println("in lecteur",scanner.Text())
			mes.retour <-scanner.Text()
		}
		file.Close()
	}
	
}

// Partie 1: récupèrent des personne_int depuis les gestionnaires, 
// font une opération dépendant de donne_statut()
// Si le statut est V, ils initialise le paquet de personne puis le repasse aux gestionnaires
// Si le statut est R, ils travaille une fois sur le paquet puis le repasse aux gestionnaires
// Si le statut est C, ils passent le paquet au collecteur
func ouvrier(ouv chan personne_int, retouv chan personne_int, colChan chan personne_int) {
	//fmt.Println("ouvrier")
	for{	
		per := <-retouv
		stat:= per.donne_statut()
		fmt.Println("in Ouvrier recieve",per)
	switch stat{ 
		case  "V" :
			fmt.Println("paquet vide in ouvrier")
			per.initialise()
			fmt.Println("((()))just test",per)//good
		/*	per.travaille()	
			fmt.Println("!!!!!!!after work",per)
			colChan<-per*/
			ouv<-per
		case "R" :
			fmt.Println("paquet R in ouvrier")
			per.travaille()
			ouv<-per
		case "C" :
			fmt.Println("paquet C in ouvrier")
			colChan<-per
		default:
			fmt.Println("default")
			colChan<-per
		//	os.Exit(1)
	}
}
	
}

// Partie 1: les producteurs cree des personne_int 
//implementees par des personne_emp initialement vides,
// de statut V mais contenant un numéro de ligne (pour etre initialisee depuis le fichier texte)
// la personne est passée aux gestionnaires
func producteur(product chan personne_int, lire chan message_lec) {
for i := 0; i < 500; i++{
	newPerson := new(personne_emp)
	newPerson.status ="V"
	nt:=make([]func (st.Personne) st.Personne,0)
	newPerson.person = pers_vide
	newPerson.ligne = rand.Intn(TAILLE_SOURCE)
	newPerson.afaire =nt
	newPerson.lec =lire
//	fmt.Println("product envoie channal :: ",newPerson.lec)
//	fmt.Println("product envoie ligne ******* :: ",newPerson.ligne)
	product <-personne_int(newPerson)
}
	
}

// Partie 2: les producteurs distants 
//cree des personne_int implementees par des personne_dist 
//qui contiennent un identifiant unique
// utilisé pour retrouver l'object sur le serveur
// la creation sur le client d'une personne_dist doit 
//declencher la creation sur le serveur d'une "gestionerFileraie"
// personne, initialement vide, de statut V
func producteur_distant(product chan personne_int,proxyChan chan message_proxy) {
	i:=1
	p:=new(personne_dist)
	p.id = i+1
	//???????????
//	localChannel :=make(chan )
//	messageProxy:=mssage_proxy(id: p.id, methodgestionerFileame:"producteur_distant")	
//	p.proxy <- messageProxy

}

// Partie 1: les gestionnaires recoivent des personne_int des producteurs
// et des ouvriers et maintiennent chacun une file de personne_int
// ils les passent aux ouvriers quand ils sont disponibles
// ATTENTION: la famine des ouvriers doit être évitée:
// si les producteurs inondent les gestionnaires de paquets, les ouvrier ne pourront
// plus rendre les paquets surlesquels ils travaillent pour en prendre des autres
func gestionnaire(product chan personne_int, ouv chan personne_int , retouv chan personne_int, gestionerFile[] personne_int ) {
	//fmt.Println("gestionnaire")
	compteur :=0
	for{
		switch{
		case compteur == TAILLE_FILE:
				//just send
				fmt.Println("# full")
				m:=gestionerFile[0]
				gestionerFile=gestionerFile[1:]
				retouv<-m
				compteur--
		case compteur == 0:
				//just recieve
				select{
				case prod :=<-ouv :
					gestionerFile=append(gestionerFile,prod)
					compteur++
				case prod:=<-product:
					gestionerFile=append(gestionerFile,prod)
					compteur++
				}
				
		case compteur ==(TAILLE_FILE/2 +1):
				//d'bord send apres  recieve
				select{
				case retouv<-gestionerFile[0]:
					gestionerFile=gestionerFile[1:]
					compteur--
				case prod :=<-ouv :
					gestionerFile=append(gestionerFile,prod)
					compteur++
				}
		default:
			select{
				//send et recieve
			case retouv<-gestionerFile[0]:
				gestionerFile=gestionerFile[1:]
				compteur--
			case prod :=<-ouv :
				gestionerFile=append(gestionerFile,prod)
				compteur++
			case prod:=<-product:
				gestionerFile=append(gestionerFile,prod)
				compteur++	
			}
		}
	}
		
	}


// Partie 1: le collecteur recoit des personne_int dont le statut est c, 
// il les collecte dans un journal
// quand il recoit un signal de fin du temps, il imprime son journal.
func collecteur( fintemps chan int , colChan chan personne_int) {
	//journal chan os.File,
	fmt.Println("+++++++++++++++++")
	journalFile, err := os.Create("journal.txt")
    if err != nil {
        fmt.Println(err)
        return
	}
	for{
		select{
		case <-fintemps:
			j := bufio.NewReader(journalFile)
			r,_ := j.ReadString('\n')
			fmt.Println(r)
		case res := <- colChan:
		//	fmt.Println("resssssssssssssssssssssssss",res)
			perString := res.vers_string()
			fmt.Println("YESYESYESperString", perString)
			journalFile.WriteString(perString)
		}
	}
}
func main() {
	rand.Seed(time.Now().UTC().UnixNano()) // graine pour l'aleatoire
	//if len(os.Args) < 3 {
	//	fmt.Println("Format: client <port> <millisecondes d'attente>")
	//	return
	//}// creaion du journal de collecteur
	// A FAIRE
	gestionerFile := make([]personne_int, 0,TAILLE_FILE)
	fintemps := make(chan int)
	// creer les canaux
	fmt.Println(">>>>start")
	retouv :=make(chan personne_int)
	ouv :=make(chan personne_int)
	lire :=make(chan message_lec)
	product :=make(chan personne_int)
	colChan:=make(chan personne_int)
	//proxyChan:=make(chan message_proxy)
	//find :=make(chan int)
for i := 0; i < 10; i++ {
	go producteur(product ,lire)
}
for i := 0; i < 12; i++ {
	go gestionnaire(product,ouv,retouv,gestionerFile)
}
for i := 0; i < 10; i++ {
	go ouvrier(ouv,retouv,colChan)
	
}
go lecteur(lire)
go collecteur(fintemps,colChan)

//partie 2
fmt.Println("second part>>>>>")

//port, _ := strconv.Atoi(os.Args[1]) // utile pour la partie 2
//millis, _ := strconv.Atoi(os.Args[2]) // duree du timeout

//go proxy(port, proxyChan, find)
	//for i:= 0; i < NB_PD; i++ {
	//	go producteur_distant(product, proxyChan)
	//}
 

	// lancer les goroutines (parties 1 et 2): 1 lecteur, 1 collecteur, des producteurs, des gestionnaires, des ouvriers
	// lancer les goroutines (partie 2): des producteurs distants, un proxy
	//port, _ := strconv.Atoi(os.Args[1]) // utile pour la partie 2
	//millis, _ := strconv.Atoi(os.Args[2]) // duree du timeout 

	//time.Sleep(time.Duration(millis) * time.Millisecond)
	fintemps <- 20000
	<-fintemps
	
}