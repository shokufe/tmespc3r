 #define paquetVide={Nom: "", Prenom: "", Age: 0, Sexe: "M"}
 #define TAILLE_FILE = 10 
 mtype {PROD,GEST,OUVR,COLLEC,FIN};
  typedef message_lec {
	 int contenu 
	 chan int retour 
 };
 typedef st.Personne{
     int Nom;
     int Prenom;
     int Age;
     int Sexe;
 };
 typedef PAQUET
{
    st.Personne person;
	int status;
	int afaire [N];
	int ligne; 
    byte  Field2;
    chan lec = [0] of {message_lec};
};

  //define channals
    chan fintemps =[0] of {int};
    chan retouv =[0] of {PAQUET};
    chan ouv =[0] of {PAQUET};
    chan lire =[0] of {message_lec};
    chan product =[0] of {PAQUET};
    chan colChan =[0] of {bool};

inline initialise(per personne_int){
	chan resultChan =[0] of{int};
	int line;
    line=p.ligne;
	message_lec mess;
    mess=message_lec{contenu: line, retour: resultChan};
	//fmt.Println("line sent", mess.contenu)
	per.lec?mess
	resultChan!li;
	per.person = personne_de_ligne(li)
	for (i=0; i< nr+1 ; i++ ){
		p.afaire [i] =  tr.UnTravail();
	}
	per.status = "R"

}
inline done_stat(per personne_int){
   per.status;
}
inline travail(){
    printf ("travail");
}
active proctype paquet(){
    byte nr;

proctype lecteur(lire chan message_lec)
{
    do
        ::lire?mes
    od;
};
proctype producteur(product chan personne_int, lire chan message_lec)
{
 do
    :: PAQUET newperson;
    :: newperson.person = paquetVide;
    :: newperson.status ='v';
    ::newperson.afaires = int new[10]
    :: newperson.ligne= product !3;   
    :: newperson.lec = lire;  
    ::  product!person ;
    od;
};
proctype gestionaire(product chan personne_int, ouv chan personne_int , retouv chan personne_int, gestionerFile[] personne_int )
{
    int compt;
  do
    ::if 
        ::compt==TAILLE_FILE ->m=gestionerFile[0];
            int i;
            int j;
            for(i:1..TAILLE_FILE){
                j=i-1;
                gestionerFile[j]=gestionerFile[i];
            }
            retouv!m;
            compt--;
    fi
    ::else
        ::if 
            ::compt==0 -> 
                ::ouv?prod :
					gestionerFile[compt]=prod;
					compt++;
                ::product?prod:
                    gestionerFile[compt+1]=prod;
                    compt++;        
     fi
    ::else
        ::if
            ::comp== TAILLE_FILE/2 +1 -> 
				retouv!gestionerFile[0];
					 for(i:1..TAILLE_FILE){
                        j=i-1;
                        gestionerFile[j]=gestionerFile[i];
                        }
					compt--
				ouv?prod ;
					gestionerFile[compt+1]=prod;
					compt++
	fi  
    od;
};
proctype ouvrier(ouv chan personne_int, retouv chan personne_int, colChan chan personne_int)
{
 do
    :: retouv?per;
    ::stat = per.status
    //get status
    :: if
        ::stat == 'V' ->per.status ='R';
            ouv!per;
        ::stat == 'R' ->goto per.travail(per);
            ouv!per;
        ::stat == 'C' ->colChan!per ;
        ::else ->printf("error"); 
        fi
od;
};
proctype collecteur(fintemps chan int , colChan chan bool)
{
    cpt = 0
	do
		::colChan? true -> 
			cpt = cpt + 1
            goto end
		
	od   
};
init:
    
   
    PAQUET gestionerFile[] ;

    atomic{
        run producteur(product,lire);
        run gestionaire(product,ouv,retouv,gestionerFile );
        run ouvrier(ouv,retouv,colChan);
        run lecteur();
        run collecteur(fintemps,colChan);
    }
end:
    printf(" finish job  ") ;

}