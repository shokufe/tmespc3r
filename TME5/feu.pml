mtype {ROUGE,ORANGE,VERT,INDETERMINEE};
//ajouter observe
chan observe = [0] of {mtype,bool}

active proctype feu(){
    bool clignotant = false;
    mtype couleur = INDETERMINEE;

    initial:
        //printf("__feu__")
        couleur = ORANGE;
        clignotant=true;
        observe! couleur, clignotant
        if
        ::true -> clignotant=false; goto red
        ::true -> goto initial
        fi
    red:
        atomic{couleur=ROUGE; observe! couleur,clignotant}
        couleur=ROUGE
        if
        ::true -> goto red;
        ::true -> goto green
        ::true -> goto fault
        fi
    green:
    atomic{couleur=VERT; observe! couleur,clignotant}
        couleur=VERT
        if
        ::true -> goto green
        ::true -> goto orang
        ::true -> goto fault
        fi
    orang:
    atomic{couleur=ORANGE; observe! couleur,clignotant}
        couleur=ORANGE
        if
        ::true -> goto orang
        ::true -> goto red
        ::true -> goto fault
        fi
    fault:
        clignotant = true
    fault_loop :
        couleur = ORANGE
        if
        ::true -> goto fault_loop
        fi

}
// ajouter observateur

active proctype observateur(){
    mtype precedent = INDETERMINEE 
    mtype couleur = INDETERMINEE
    bool clignotant = false
   // mtype couleur
   // printf("___observateur")
    
    do 
    ::observe? couleur, clignotant ->
        if
        ::atomic{
            couleur == ORANGE -> assert (precedent != ROUGE ); precedent=ORANGE
        }
        
         ::atomic{
            couleur == ROUGE -> assert (precedent != VERT ); precedent=ROUGE
        }
         ::atomic{
            couleur == VERT -> assert (precedent != ORANGE ); precedent=VERT
        }
        fi
    
    ::atomic {
        clignotant -> assert (couleur== ORANGE )
    }
    ::atomic{
        couleur==ROUGE ; observe!couleur,clignotant
    }
    od

}