#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "fthread.h"


#define CONSO 2
#define PROD 1
#define ME 1
#define NBCIBLE 5


ft_event_t  produce,consume;



typedef struct Paquet Paquet;
struct Paquet{
    char * nom;
    //paquet suivant pour pouvoir faire une liste de pointeur
    Paquet *suivant;
};

typedef struct Tapis Tapis;
struct Tapis {
    int capacite;
    int nbElem;
    Paquet *premier;
};

typedef struct threadProd_args threadProd_args;
struct threadProd_args {
    char * nom;
    Tapis *t;
    int cible;


};

typedef struct threadCons_args threadCons_args;
struct threadCons_args {
    int id;
    Tapis *t;
    int *compteur;


};


typedef struct threadMes_args threadMes_args;
struct threadMes_args {
    int id;
};

bool enfiler(Tapis *file, Paquet *p)
{

    if (file == NULL || p == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->premier != NULL)
    {
        if (file->nbElem <= file->capacite){
            Paquet *paquetActuel = file->premier;
            while (paquetActuel->suivant != NULL)
            {
                paquetActuel = paquetActuel->suivant;
            }   
            paquetActuel->suivant = p;
            file->nbElem ++;
        } else {
            return false;
        }

    }
    else
    {
        file->premier = p;
        file->nbElem ++;
    }
    return true;
}


//METHODE DEFILER

Paquet * defiler(Tapis *file)
{
    if (file == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (file->premier != NULL)
    {
        Paquet *paquetDefile = file->premier;
        file->premier = paquetDefile->suivant;
        file->nbElem--;

        return paquetDefile;
    }
    return NULL;
}

//THRED PRODUCT

void threadProd (void * args){
    struct threadProd_args *data;
    int numProduit = 1;
    data = args;
    int nb_cible = data->cible;

    while(1){
        sleep(0.5);
      //  pthread_mutex_lock (&mutex);

        if (nb_cible >= numProduit){
            Paquet* p = malloc(sizeof(p));

            char Output[200];
            sprintf(Output, "%s %d", data->nom, numProduit);

            p->nom = Output;
            p->suivant = NULL;
            if (enfiler(data->t, p)){
                numProduit += 1;
//SIGNAL
                ft_thread_generate(produce);
            }else{
              ft_thread_await(consume);
                ft_thread_cooperate();
            }
        }

      //  pthread_cond_signal(&condition);
        //pthread_mutex_unlock (&mutex);
    }

}

//THREAD CONSUM

void  threadConsum(void * args){
    struct threadCons_args *data;
    data = args;

    while(1){
        if (*data->compteur > 0){
            sleep(0.5);
          //  pthread_mutex_lock (&mutex);
            Paquet *p = defiler(data->t);

            if (p != NULL){
                printf("C%d mange %s\n", data->id, p->nom);

                *data->compteur = *data->compteur - 1;
              //  ft_thread_signal();
                ft_thread_generate(consume);
                sleep(0.5);
            }else{
             ft_thread_await(produce);
               ft_thread_cooperate();

            }
          //  pthread_cond_signal(&condition);
            //pthread_mutex_unlock (&mutex);
        }


    }

  }
//Messager



//void  *threadMessag(void * args){
//    struct threadCons_args *data;
//    int cpt=threadComsum();
//     printf("C%d cpt %s\n", cpt);
//    data = args;
//        if (cpt > 0){
//            sleep(0.5);
//            Paquet *p = defiler(data->t);
//
//            if (p != NULL){
//                printf("C%d mange %s\n", data->id, p->nom);
//
//                cpt = cpt - 1;
//                ft_thread_generate(consume);
//              //  free(p);
//                sleep(0.5);
//            }else{
//             ft_thread_await(produce);
//
//            }
//
//        }
//        ft_thread_cooperate();
//
//        if (nb_cible >= numProduit){
//            Paquet* p = malloc(sizeof(p));
//
//            char Output[200];
//            sprintf(Output, "%s %d", data->nom, numProduit);
//
//            p->nom = Output;
//            p->suivant = NULL;
//            if (enfiler(data->t, p)){
//                numProduit += 1;
//                ft_thread_generate(produce);
//            }else{
//              ft_thread_await(consume);
//            }
//        }
//        ft_thread_cooperate();
//
//        return Null;
// }
//
//

int main(int argc, char *argv[]) {

  ft_scheduler_t consomateur  =   ft_scheduler_create();
  ft_scheduler_t producteur  =   ft_scheduler_create();
  ft_thread_t product,consumate,mess;

  consume=ft_event_create(consomateur);
  produce=ft_event_create(producteur);

    int i;
    Tapis* t = malloc(sizeof(t));
    t->capacite = 10;
    t->premier = NULL;
    int compteur = NBCIBLE * PROD;

    threadProd_args argsP [PROD];

    for(i=0; i < PROD; i++){
        argsP[i].nom = "Fruit";
        argsP[i].t = t;
        argsP[i].cible = NBCIBLE;
      //  pthread_create(&leThreadProduct[i], NULL, threadProd, (void*) &argsP[i]);
         product=ft_thread_create(producteur,&threadProd,NULL,(void*) &argsP[i]);
       printf("Votre chaine de caractères ici");
        ft_scheduler_start(producteur);
        ft_thread_join(product);



    }



printf("Votre chaine de caractères ici");
    threadCons_args argsC [CONSO];

    for(i=0; i < CONSO; i++){
        argsC[i].id = i+1;
        argsC[i].t = t;
        argsC[i].compteur = &compteur;
      //  pthread_create(&leThreadConsum[i], NULL, threadComsum, (void*) &argsC[i]);
      consumate  =ft_thread_create(consomateur,threadConsum,NULL,(void*) &argsC[i]);
      ft_scheduler_start(consomateur);
        ft_thread_join(consumate);


    }






            //il se link au producteur pour consommer  puis il se delink
          ft_thread_link (producteur);
         mess  =ft_thread_create(producteur,threadConsum,NULL,NULL);
        ft_scheduler_start(producteur);
          ft_thread_join(mess);
         ft_thread_unlink ();


        //  pthread_create(&leThreadConsum[i], NULL, threadComsum, (void*) &argsC[i]);
        //il se link au consomateur pour produire  puis il se delimk
        ft_thread_link (consomateur);
       mess  =ft_thread_create(consomateur,threadProd,NULL,NULL);
       ft_scheduler_start(consomateur);
         ft_thread_join(mess);
       ft_thread_unlink ();

       return 0;


}
